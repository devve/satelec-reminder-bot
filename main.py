import os
import datetime
import time
import logging
from random import randint

from models import session
from models import events
from models import email

import apprise
from dotenv import load_dotenv


if __name__ == "__main__":

    # Configure logging
    log_level = os.environ.get("LOG_LEVEL", "INFO")
    logging.basicConfig(
        filename="reminder-bot.logs",
        format="%(asctime)s %(levelname)s %(message)s",
        datefmt="%d/%m/%Y %I:%M:%S",
        level=log_level,
    )
    logger = logging.getLogger(__name__)

    load_dotenv()

    apobj = apprise.Apprise()
    bot_token = os.environ.get("TELEGRAM_BOT_TOKEN")
    chat_id = os.environ.get("TELEGRAM_CHAT_ID")
    apobj.add(f"tgram://{bot_token}/{chat_id}")

    s = session.Session()
    try:
        token = s.get_jwt()
    except Exception as e:
        apobj.notify(
            body="No he podido mandar el recordatorio. 🥺",
            title=f"Error",
        )
    e = events.Events(token)
    tomorrow = datetime.date.today() + datetime.timedelta(days=1)
    events_on_date = e.get_events_with_assistants(date=tomorrow)
    logger.debug("Found this events:")
    logger.debug(events_on_date)
    for event in events_on_date:
        logger.info(event["title"])
        logger.info(event["date"])
        logger.info("Sending the emails")
        for assistant in event["assistants"]:
            m = email.Email(
                "[Satelec] Recordatorio de la actividad de mañana",
                assistant["email"],
                assistant["name"],
                assistant["surname"],
                event["title"],
                event["location"],
                event["date"],
                event["url"],
                cv=assistant["cv"],
            )
            m.send()
            time.sleep(randint(1, 4))
        apobj.notify(
            body=f"Participantes: {len(event['assistants']) - 1}",
            title=f"Recordatorios enviados para {event['title']}",
        )
    logger.info("Finished")
