import os
import smtplib
import ssl
import logging


class Email:
    def __init__(self, subject, to, name, surname, title, location, dt, url, cv=False):
        self.subject = subject
        self.to = to
        self.name = name
        self.surname = surname
        self.title = title
        self.location = location
        self.dt = dt
        self.url = url
        self.cv = cv

        self.sender = os.environ.get("MAIL_SENDER", None)
        self.password = os.environ.get("MAIL_SENDER_PASSWORD", None)
        self.smtp_server = os.environ.get("SMTP_SERVER", None)
        self.smtp_port = int(os.environ.get("SMTP_PORT", 465))

        # Configure logging
        log_level = os.environ.get("LOG_LEVEL", "INFO")
        logging.basicConfig(
            filename="reminder-bot.logs",
            format="%(asctime)s %(levelname)s %(message)s",
            datefmt="%d/%m/%Y %I:%M:%S",
            level=log_level,
        )
        self.logger = logging.getLogger(__name__)

    def cv_reminder(self):
        if not self.cv:
            return "Aún no has subido el CV a la plataforma. Te recomendamos subirlo para conseguir hasta 2 ECTS, acceder a ofertas de trabajo y participar en sorteos."
        else:
            return ""

    def generate_message(self):
        return f"""Subject: {self.subject}

Hola {self.name},

Desde Satelec, te recordamos que mañana, día {self.dt.strftime("%d a las %H:%M")}, te esperamos en la actividad "{self.title}" a la que te has apuntado. Tendrá lugar en {self.location}.

Te rogamos que vayas a la puerta con 5 minutos de antelación para registrar tu asistencia.

{self.cv_reminder()}

Puedes encontrar toda la información en el siguiente enlace: {self.url}


Un saludo,
el equipo de Satelec.

    """

    def send(self):
        self.logger.info(f"Sending email to {self.name}")
        message = self.generate_message()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(
            self.smtp_server, self.smtp_port, context=context
        ) as server:
            server.login(self.sender, self.password)
            server.sendmail(
                f"Satelec 2023 <{self.sender}>", self.to, message.encode("utf8")
            )
