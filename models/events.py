import os
import logging
import requests
import datetime

import openpyxl


class Events:
    def __init__(self, token):
        self.url = os.environ.get("SATELEC_URL")
        self.token = token

        # Configure logging
        log_level = os.environ.get("LOG_LEVEL", "INFO")
        logging.basicConfig(
            filename="reminder-bot.logs",
            format="%(asctime)s %(levelname)s %(message)s",
            datefmt="%d/%m/%Y %I:%M:%S",
            level=log_level,
        )
        self.logger = logging.getLogger(__name__)

    def get_events(self, date=None):
        response = requests.get(
            f"{self.url}/api/activity/future",
            headers={"token": self.token},
        )
        events = response.json().get("activities", [])

        if date is not None:
            events = [
                e
                for e in events
                if datetime.datetime.strptime(e["date"], "%Y-%m-%dT%H:%M:%S.%fZ").date()
                == date
            ]
        return [
            {
                "id": event["_id"],
                "title": event["title"],
                "date": datetime.datetime.strptime(
                    event["date"], "%Y-%m-%dT%H:%M:%S.%fZ"
                )
                + datetime.timedelta(hours=1),
                "location": event["location"],
                "url": f"{self.url}/activity/{event['_id']}",
            }
            for event in events
        ]

    def get_event_assistants(self, event_id):
        assistants = [
            {
                "name": "Satelec",
                "surname": "All",
                "email": "all@satelecupm.com",
                "cv": False,
            }
        ]
        response = requests.get(
            f"{self.url}/api/activity/{event_id}/subscriptions/live/download",
            headers={"token": self.token},
            cookies={"token": self.token},
        )

        with open(f"/tmp/{event_id}.xlsx", "wb") as f:
            f.write(response.content)

        workbook = openpyxl.load_workbook(f"/tmp/{event_id}.xlsx")
        worksheet = workbook.active
        for row in worksheet.iter_rows(
            min_row=2, max_col=4, max_row=100, values_only=True
        ):
            if row[0] is None:
                break
            assistants.append(
                {
                    "name": row[0],
                    "surname": row[1],
                    "email": row[2],
                    "cv": True if row[3] is not None else False,
                }
            )
        os.remove(f"/tmp/{event_id}.xlsx")
        return assistants

    def get_events_with_assistants(self, date=None):
        events = self.get_events(date=date)
        for event in events:
            event["assistants"] = self.get_event_assistants(event["id"])
        return events
