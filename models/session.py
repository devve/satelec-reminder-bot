import os
import logging
import requests


class Session:
    def __init__(self):
        self.email = os.environ.get("SATELEC_USER")
        self.password_hash = os.environ.get("SATELEC_PASSWORD_HASH")
        self.url = os.environ.get("SATELEC_URL")

        # Configure logging
        log_level = os.environ.get("LOG_LEVEL", "INFO")
        logging.basicConfig(
            filename="reminder-bot.logs",
            format="%(asctime)s %(levelname)s %(message)s",
            datefmt="%d/%m/%Y %I:%M:%S",
            level=log_level,
        )
        self.logger = logging.getLogger(__name__)

    def get_jwt(self):
        response = requests.post(
            f"{self.url}/api/auth/login",
            json={"email": self.email, "password": self.password_hash},
        )
        return response.json().get("token")
